import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart' as notification;
import 'package:mailer/mailer.dart';
import 'package:mailer/smtp_server.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:share_plus/share_plus.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

notification.FlutterLocalNotificationsPlugin? flutterLocalNotificationsPlugin;

String? selectedNotificationPayload;

class PDFView extends StatefulWidget {
  String? document;
  String? file;

  PDFView({Key? key, this.document, this.file}) : super(key: key);

  @override
  State<PDFView> createState() => _PDFViewState();
}

class _PDFViewState extends State<PDFView> {
  late bool isComplete;

  @override
  void initState() {
    super.initState();
    isComplete = false;
    flutterLocalNotificationsPlugin =
        notification.FlutterLocalNotificationsPlugin();
    var android =
        const notification.AndroidInitializationSettings('@mipmap/ic_launcher');
    var iOSSettings = const notification.IOSInitializationSettings(
      requestSoundPermission: false,
      requestBadgePermission: false,
      requestAlertPermission: false,
    );
    final initSettings =
        notification.InitializationSettings(android: android, iOS: iOSSettings);

    flutterLocalNotificationsPlugin!
        .initialize(initSettings, onSelectNotification: _onSelectNotification);
  }

  Future<void> _onSelectNotification(String? json) async {
    final obj = jsonDecode(json!);

    if (obj['isSuccess']) {
      OpenFile.open(obj['filePath']);
    } else {
      showDialog(
        context: context,
        builder: (_) => AlertDialog(
          title: const Text('Error'),
          content: Text('${obj['error']}'),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: const Text("PDF View File"),
          backgroundColor: Colors.deepPurpleAccent,
          actions: [
            Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 8),
                  child: InkWell(
                      child: isComplete
                          ? const Center(
                              child: SizedBox(
                                  height: 30,
                                  width: 30,
                                  child: CircularProgressIndicator(
                                    color: Colors.white,
                                  )),
                            )
                          : const Icon(Icons.send),
                      onTap: () async {
                        Map<Permission, PermissionStatus> statuses = await [
                          Permission.storage,
                          Permission.manageExternalStorage,
                        ].request();
                        if (statuses[Permission.storage]!.isGranted) {
                          if (Platform.isAndroid) {
                            getDownloadData();
                          } else {
                            var dir = await getApplicationDocumentsDirectory();
                            String filename = widget.file!.split('/').last;
                            String savePath = dir.path + "/$filename";
                            print(savePath);
                            print(filename);
                            try {
                              await _startDownload(
                                savePath,
                              );
                              print("File is saved to download folder.");
                            } on DioError catch (e) {
                              print(e.message);
                            }
                          }
                        }
                      }),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 8),
                  child: InkWell(
                      child: const Icon(Icons.outgoing_mail),
                      onTap: () async {
                        sendEmail();
                      }),
                ),
              ],
            ),
          ],
        ),
        body: widget.document != null
            ? SfPdfViewer.network(widget.document!.toString())
            : Container(),
      ),
    );
  }

  sendEmail() async {
    final user = await GoogleAuthApi.signIn();
    if (user == null) return;
    final email = user.email;
    final auth = await user.authentication;
    final token = auth.accessToken!;

    print('Authentication: $email');

    final smtpServer = gmailSaslXoauth2(email, token);
    final dir = await Directory('/storage/emulated/0/Download');
    String filename = widget.file!.split('/').last;
    File savePath = File(dir.path + "/$filename");
    final message = Message()
      ..from = Address(email, 'Kamini')
      ..recipients.add('kamini.p@tridhya.com')
      ..subject = 'Hello kamini'
      ..text = 'This is a test email'
      ..attachments.add(FileAttachment(File(savePath.path)));
    try {
      await send(message, smtpServer);
      showSnackBar('sent email successfully');
    } on MailerException catch (e) {
      print(e);
    }
  }

  void showSnackBar(String text) {
    final snackBar = SnackBar(
      content: Text(
        text,
        style: const TextStyle(fontSize: 16, color: Colors.white),
      ),
      backgroundColor: Colors.black,
    );
    ScaffoldMessenger.of(context)
      ..removeCurrentSnackBar()
      ..showSnackBar(snackBar);
  }

  getDownloadData() async {
    setState(() {
      isComplete = true;
    });
    final dir = await Directory('/storage/emulated/0/Download');
    if (dir != null) {
      String filename = widget.file!.split('/').last;
      File savePath = File(dir.path + "/$filename");
      print(savePath);
      print(filename);

      try {
        if (!await savePath.exists()) {
          await _startDownload(savePath.path);
        }
        print("File is saved to download folder.");
      } on DioError catch (e) {
        print(e.message);
      }

      if (await savePath.exists()) {
        Share.shareFiles([savePath.path],
            subject: "PDF File Share", text: "Hello");
      }
    }
    setState(() {
      isComplete = false;
    });
  }

  Future<void> _startDownload(String savePath) async {
    Map<String, dynamic> result = {
      'isSuccess': false,
      'filePath': null,
      'error': null,
    };

    try {
      final response = await Dio().download(widget.file!.toString(), savePath,
          onReceiveProgress: _onReceiveProgress);
      result['isSuccess'] = response.statusCode == 200;
      result['filePath'] = savePath;
    } catch (ex) {
      result['error'] = ex.toString();
    } finally {
      await _showNotification(result);
    }
  }

  void _onReceiveProgress(int received, int total) {
    if (total != -1) {
      setState(() {});
    }
  }

  Future<void> _showNotification(Map<String, dynamic> downloadStatus) async {
    var android = const notification.AndroidNotificationDetails(
        'channel id', 'channel name',
        priority: notification.Priority.high,
        importance: notification.Importance.max);
    const iOS = notification.IOSNotificationDetails();
    final platform =
        notification.NotificationDetails(android: android, iOS: iOS);
    final json = jsonEncode(downloadStatus);
    final isSuccess = downloadStatus['isSuccess'];

    await flutterLocalNotificationsPlugin!.show(
        0, // notification id
        isSuccess ? 'Success' : 'Failure',
        isSuccess
            ? 'File has been downloaded successfully!'
            : 'There was an error while downloading the file.',
        platform,
        payload: json);
  }
}

class GoogleAuthApi {
  static final _googleSignIn =
      GoogleSignIn(scopes: ['https://mail.google.com/']);

  static Future<GoogleSignInAccount?> signIn() async {
    if (await _googleSignIn.isSignedIn()) {
      return _googleSignIn.currentUser;
    } else {
      return await _googleSignIn.signIn();
    }
  }

  static Future signOut() => _googleSignIn.signOut();
}
