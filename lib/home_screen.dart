import 'package:flutter/material.dart';
import 'package:pdf_download/pdfview_screen.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
import 'model.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  SfPdfViewer? document;
  List<Url> data = [
    Url("https://fluttercampus.com/sample.pdf"),
    Url("https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf"),
    Url("https://cdn.syncfusion.com/content/PDFViewer/flutter-succinctly.pdf"),
  ];

  changePDF(index) {
    document = SfPdfViewer.network(data[index].fileurl!.toString(),
        canShowPaginationDialog: false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: const Text("PDF Download File"),
          backgroundColor: Colors.deepPurpleAccent,
        ),
        body: Center(
          child: Padding(
            padding: const EdgeInsets.all(8),
            child: ListView.builder(
              shrinkWrap: true,
              itemCount: data.length,
              itemBuilder: (context, index) {
                return Card(
                  color: Colors.grey,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Expanded(
                            child: Text(
                          data[index].fileurl.toString(),
                          style: const TextStyle(color: Colors.black),
                        )),
                        InkWell(
                            child: const Icon(Icons.visibility),
                            onTap: () {
                              changePDF(index);
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => PDFView(
                                          document:
                                              data[index].fileurl.toString(),
                                          file: data[index].fileurl.toString(),
                                        )),
                              );
                              // changePDF(index);
                            }),
                        const SizedBox(
                          width: 5,
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        ));
  }
}
